package org.trueafrican.monitoringtool.dao;

import java.text.ParseException;
import java.util.List;

import org.trueafrican.monitoringtool.model.Company;

public interface CompanyDao {
	
	Company saveCompany(Company company);
    
    List<Company> findAllCompanys();
    
    List<Company> findAllCompanys(String startDate, String endDate, String companyName)  throws ParseException;
     
    void deleteCompany(Company company);
     
    Company findByCompanyId(Long companyId);
    
    Company findByCompanyName(String companyName);
     
    Company updateCompany(Company company);
}