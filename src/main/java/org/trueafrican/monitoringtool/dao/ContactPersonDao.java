package org.trueafrican.monitoringtool.dao;

import java.text.ParseException;
import java.util.List;

import org.trueafrican.monitoringtool.model.ContactPerson;

public interface ContactPersonDao {
	
	void saveContactPerson(ContactPerson contactPerson);
    
    List<ContactPerson> findAllContactPersons();
    
    List<ContactPerson> findAllContactPersons(String startDate, String endDate, String name) throws ParseException;
     
    void deleteContactPerson(ContactPerson contactPerson);
     
    ContactPerson findByContactPersonId(String contactPersonId);
    
    ContactPerson findContactByCompanyId(String companyId);
     
    void updateContactPerson(ContactPerson contactPerson);
}