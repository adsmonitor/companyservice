package org.trueafrican.monitoringtool.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.CompanyDao;
import org.trueafrican.monitoringtool.model.Company;

@Transactional
@Repository("companyDao")
public class CompanyDaoImpl extends AbstractDao implements CompanyDao {

	@Override
	public void deleteCompany(Company company) {
		delete(company);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Company> findAllCompanys() {
		Criteria criteria = getSession().createCriteria(Company.class);
		List<Company> companys = (List<Company>) criteria.list();
        return companys;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Company> findAllCompanys(String startDate, String endDate, String companyName) throws ParseException {
		Criteria criteria = getSession().createCriteria(Company.class);
		
		if (startDate != null && endDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startdate = formatter.parse(startDate);
			Date enddate = formatter.parse(endDate);
			criteria.add(Restrictions.between("dateCreated", startdate, enddate));
		} else {
			if (startDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date startdate = formatter.parse(startDate);
				criteria.add(Restrictions.ge("dateCreated", startdate));
			}
			
			if (endDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date enddate = formatter.parse(endDate);
				criteria.add(Restrictions.le("dateCreated", enddate));
			}
		}
		
		if (companyName != null) {
			criteria.add(Restrictions.like("companyName", "%" + companyName + "%"));
		}
		
		List companys = criteria.list();
		if (companys == null) {
			return new ArrayList<Company>();
		} 
        return (List<Company>) companys;
	}

	@Override
	public Company findByCompanyId(Long companyId) {
		Criteria criteria = getSession().createCriteria(Company.class);
		
		if (companyId != null) {
			criteria.add(Restrictions.eq("companyId", companyId));
		}
        return (Company)criteria.uniqueResult();
	}

	@Override
	public Company saveCompany(Company company) {
		save(company);
		Query query = getSession().createQuery("FROM Company ORDER BY companyId DESC");
		query.setMaxResults(1);
        return (Company)query.uniqueResult();
	}

	@Override
	public Company updateCompany(Company company) {
		saveOrUpdate(company);
		return findByCompanyId(company.getCompanyId());
	}

	@Override
	public Company findByCompanyName(String companyName) {
		Criteria criteria = getSession().createCriteria(Company.class);
		
		if (companyName != null) {
			criteria.add(Restrictions.eq("companyName", companyName));
		}
        return (Company)criteria.uniqueResult();
	}
}