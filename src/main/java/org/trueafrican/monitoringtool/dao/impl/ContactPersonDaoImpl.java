package org.trueafrican.monitoringtool.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.AbstractDao;
import org.trueafrican.monitoringtool.dao.ContactPersonDao;
import org.trueafrican.monitoringtool.model.ContactPerson;

@Transactional
@Repository("contactPersonDao")
public class ContactPersonDaoImpl extends AbstractDao implements ContactPersonDao {

	@Override
	public void deleteContactPerson(ContactPerson contactPerson) {
		delete(contactPerson);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactPerson> findAllContactPersons() {
		Criteria criteria = getSession().createCriteria(ContactPerson.class);
		List<ContactPerson> contacts = (List<ContactPerson>) criteria.list();
        return contacts;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactPerson> findAllContactPersons(String startDate, String endDate, String name) throws ParseException {
		Criteria criteria = getSession().createCriteria(ContactPerson.class);
		
		if (startDate != null && endDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startdate = formatter.parse(startDate);
			Date enddate = formatter.parse(endDate);
			criteria.add(Restrictions.between("dateCreated", startdate, enddate));
		} else {
			if (startDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date startdate = formatter.parse(startDate);
				criteria.add(Restrictions.ge("dateCreated", startdate));
			}
			
			if (endDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				Date enddate = formatter.parse(endDate);
				criteria.add(Restrictions.le("dateCreated", enddate));
			}
		}
		
		if (name != null) {
			criteria.add(Restrictions.like("firstName", "%" + name + "%"));
			criteria.add(Restrictions.like("lastName", "%" + name + "%"));
		}
		
		List contacts = criteria.list();
		if (contacts == null) {
			return new ArrayList<ContactPerson>();
		} 
        return (List<ContactPerson>) contacts;
	}

	@Override
	public ContactPerson findByContactPersonId(String contactPersonId) {
		Criteria criteria = getSession().createCriteria(ContactPerson.class);
		
		if (contactPersonId != null) {
			criteria.add(Restrictions.eq("contactId", new Long(contactPersonId)));
		}
        return (ContactPerson)criteria.uniqueResult();
	}

	@Override
	public void saveContactPerson(ContactPerson contactPerson) {
		save(contactPerson);
	}

	@Override
	public void updateContactPerson(ContactPerson contactPerson) {
		saveOrUpdate(contactPerson);
	}

	@Override
	public ContactPerson findContactByCompanyId(String companyId) {
		Criteria criteria = getSession().createCriteria(ContactPerson.class);
		
		if (companyId != null) {
			criteria.add(Restrictions.eq("companyId", new Long(companyId)));
		}
        return (ContactPerson)criteria.uniqueResult();
	}
}