package org.trueafrican.monitoringtool.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.trueafrican.monitoringtool.model.Company;
import org.trueafrican.monitoringtool.model.ContactPerson;
import org.trueafrican.monitoringtool.model.Response;
import org.trueafrican.monitoringtool.service.CompanyService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CompanyController {
	
	private static final String hostUrl = "http://localhost:8080";
	
	@Autowired
	CompanyService companyService;
	
    public CompanyController() {}
	
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
 
    @DeleteMapping("/del_company/{id}")
	public ResponseEntity<Response> deleteCompany(@PathVariable(value = "id") Long companyId) {
		Company company = companyService.findByCompanyId(companyId);
	    if(company == null) {
	        return ResponseEntity.notFound().build();
	    }
	    
	    companyService.deleteCompany(company);
	    return ResponseEntity.ok(new Response("Company has been deleted successfully", "Success"));
	}

    @RequestMapping(value = "/companys", method = RequestMethod.GET)
	public @ResponseBody List<Company> findAllCompanys() {
		return companyService.findAllCompanys();
	}
    
    /*@RequestMapping(value = "/companys", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody List<Company> findAllCompanys() {
		return companyService.findAllCompanys();
	}*/

    @GetMapping("/companys/{startDate}/{endDate}/{companyName}")
	public List<Company> findAllCompanys(@PathVariable(value = "startDate") String startDate, @PathVariable(value = "endDate") String endDate, @PathVariable(value = "companyName") String companyName) {
		return companyService.findAllCompanys(startDate, endDate, companyName);
	}

    @GetMapping("/company/{companyId}")
	public Company findByCompanyId(@PathVariable(value = "companyId") Long companyId) {
		return companyService.findByCompanyId(companyId);
	}

    @PostMapping("/newCompany")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<Company> saveCompany(@RequestBody String companyInfo) throws JsonParseException, IOException {
    	companyInfo = java.net.URLDecoder.decode(companyInfo, "UTF-8");
    	companyInfo = companyInfo.replaceAll("=", "");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	Company company = mapper.readValue(companyInfo, Company.class);
    	company.setDateCreated(new Date());
    	company = new Company(companyService.saveCompany(company));
		
		if(company == null) {
            return ResponseEntity.notFound().build();
        }
		return ResponseEntity.ok(company);
	}
    
    @PostMapping("/addContact")
    @CrossOrigin(origins = hostUrl)
	public ResponseEntity<ContactPerson> addContact(@RequestBody String contactInfo) throws JsonParseException, IOException {
    	contactInfo = java.net.URLDecoder.decode(contactInfo, "UTF-8");
		contactInfo = contactInfo.replaceAll("=", "");
		
    	ObjectMapper mapper = new ObjectMapper();
    	ContactPerson contactPerson = mapper.readValue(contactInfo, ContactPerson.class);
    	contactPerson.setDateCreated(new Date());
    	companyService.saveContactPerson(contactPerson);		
		return ResponseEntity.ok().build();
	}

    //TODO return company object does not include the old adverts that were saved earlier..
    @PutMapping("/updateCompany/{companyId}")
	public ResponseEntity<Company> updateCompany(@PathVariable(value = "companyId") Long companyId, @Valid @RequestBody Company updatedCompany) {
        if(companyService.findByCompanyId(companyId) == null) {
            return ResponseEntity.notFound().build();
        }
        Company company = new Company(updatedCompany);
        company = companyService.updateCompany(company);
        return ResponseEntity.ok(company);
	}
    
    @GetMapping("/getContactInfo/{contactId}")
	public ContactPerson findContactById(@PathVariable(value = "contactId") String contactId) {
		return companyService.findByContactPersonId(contactId);
	}
    
    @GetMapping("/getContactByCompanyId/{companyId}")
	public ContactPerson findContactByCompanyId(@PathVariable(value = "companyId") String companyId) {
		return companyService.findContactByCompanyId(companyId);
	}
}