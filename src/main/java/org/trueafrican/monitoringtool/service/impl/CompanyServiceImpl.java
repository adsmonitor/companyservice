package org.trueafrican.monitoringtool.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trueafrican.monitoringtool.dao.CompanyDao;
import org.trueafrican.monitoringtool.dao.ContactPersonDao;
import org.trueafrican.monitoringtool.model.Company;
import org.trueafrican.monitoringtool.model.ContactPerson;
import org.trueafrican.monitoringtool.service.CompanyService;

@Service("companyService")
@Transactional
public class CompanyServiceImpl implements CompanyService {

	@Autowired
    private CompanyDao companyDao;
	
	@Autowired
    private ContactPersonDao contactPersonDao;
	
	public CompanyServiceImpl() {}
	
    public CompanyServiceImpl(CompanyDao companyDao) {
        this.companyDao = companyDao;
    }
	
	@Override
	public void deleteCompany(Company company) {
		companyDao.deleteCompany(company);
	}

	@Override
	public List<Company> findAllCompanys() {
		List<Company> companys = companyDao.findAllCompanys();
		List<Company> newList = new ArrayList<Company>();
		 for (Company company : companys) {
			 Company companyDto = new Company(company);
			 newList.add(companyDto);
		 }
		 return newList;
	}

	@Override
	public List<Company> findAllCompanys(String startDate, String endDate, String companyName) {
		List<Company> companys = null;
		try {
			companys = companyDao.findAllCompanys(startDate, endDate, companyName);
		} catch (ParseException e) {
			
		}
		List<Company> newList = new ArrayList<Company>();
		 for (Company company : companys) {
			 Company companyDto = new Company(company);
			 newList.add(companyDto);
		 }
		 return newList;
	}

	@Override
	public Company findByCompanyId(Long companyId) {
		Company company = companyDao.findByCompanyId(companyId);
		if (company != null) {
			return new Company(company);
		}
		return null;
	}

	@Override
	public Company saveCompany(Company company) {
		Company companyDto = companyDao.saveCompany(company);
		if (companyDto != null) {
			return new Company(companyDto);
		}
		return null;
	}

	@Override
	public Company updateCompany(Company company) {
		Company companyDto = companyDao.updateCompany(company);
		if (companyDto != null) {
			return new Company(companyDto);
		}
		return null;
	}

	@Override
	public Company findByCompanyName(String companyName) {
		Company company = companyDao.findByCompanyName(companyName);
		if (company != null) {
			return new Company(company);
		}
		return null;
	}

	@Override
	public ContactPerson findByContactPersonId(String contactPersonId) {
		return contactPersonDao.findByContactPersonId(contactPersonId);
	}

	@Override
	public void updateContactPerson(ContactPerson contactPerson) {
		contactPersonDao.updateContactPerson(contactPerson);
	}

	@Override
	public ContactPerson findContactByCompanyId(String companyId) {
		return contactPersonDao.findContactByCompanyId(companyId);
	}

	@Override
	public void saveContactPerson(ContactPerson contactPerson) {
		contactPersonDao.saveContactPerson(contactPerson);
	}
}