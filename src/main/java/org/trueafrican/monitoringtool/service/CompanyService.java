package org.trueafrican.monitoringtool.service;

import java.util.List;

import org.trueafrican.monitoringtool.model.Company;
import org.trueafrican.monitoringtool.model.ContactPerson;

public interface CompanyService {

	Company saveCompany(Company company);
    
    List<Company> findAllCompanys();
    
    List<Company> findAllCompanys(String startDate, String endDate, String companyName);
     
    void deleteCompany(Company company);
     
    Company findByCompanyId(Long companyId);
    
    Company findByCompanyName(String companyName);
     
    Company updateCompany(Company company);
    
    ContactPerson findByContactPersonId(String contactPersonId);
    
    void updateContactPerson(ContactPerson contactPerson);
    
    ContactPerson findContactByCompanyId(String companyId);
    
    void saveContactPerson(ContactPerson contactPerson);
}